#a function to convert lowercase to upper case
def to_uppercase(input_string):
    upper=""
    for i in input_string:
        if 'a'<=i<='z':
            upper=upper+chr(ord(i)-32)
        else:
            upper+=i
    return upper
strings=input("enter a string in lowercase")
upper=to_uppercase(strings)
print(upper)